package micronaut.kafa.streams.example.stream;

import java.util.Arrays;
import java.util.Properties;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Grouped;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;

import io.micronaut.configuration.kafka.streams.ConfiguredStreamBuilder;
import io.micronaut.context.annotation.Factory;
import micronaut.kafa.streams.example.kafka.constants.KafkaConstants;

@Factory
public class WordCountStream {

  @Singleton
  @Named(KafkaConstants.STREAM_WORD_COUNT)
  KStream<String, String> wordCountStream(ConfiguredStreamBuilder configuredStreamBuilder) {
    Properties properties = configuredStreamBuilder.getConfiguration();
    properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
    properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
    properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
    KStream<String, String> source = configuredStreamBuilder.stream(KafkaConstants.INPUT_TOPIC);
    KTable<String, Long> wordCounts = source
        .flatMapValues(value -> Arrays.asList(value.toLowerCase().split("\\W+")))
        .groupBy((key, word) -> word, Grouped.with(Serdes.String(), Serdes.String()))
        //Store the result in a store for lookup later
        .count(Materialized.as(KafkaConstants.WORD_COUNT_STORE));
    wordCounts.toStream().to(KafkaConstants.OUTPUT_TOPIC, Produced.with(Serdes.String(), Serdes.Long()));
    return source;
  }

}

