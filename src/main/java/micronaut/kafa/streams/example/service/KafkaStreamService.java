package micronaut.kafa.streams.example.service;

import micronaut.kafa.streams.example.entities.KafkaMessageEntity;

public interface KafkaStreamService {

  void sendKafkaMessage(KafkaMessageEntity kafkaMessageEntity);

}
