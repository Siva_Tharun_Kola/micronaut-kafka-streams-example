package micronaut.kafa.streams.example.service;

import javax.inject.Inject;
import javax.inject.Singleton;

import micronaut.kafa.streams.example.entities.KafkaMessageEntity;
import micronaut.kafa.streams.example.kafka.InputTopicProducerClient;

@Singleton
public class KafkaStreamServiceImpl implements KafkaStreamService {

  private final InputTopicProducerClient inputTopicProducerClient;

  @Inject
  public KafkaStreamServiceImpl(InputTopicProducerClient inputTopicProducerClient) {
    this.inputTopicProducerClient = inputTopicProducerClient;
  }

  @Override
  public void sendKafkaMessage(KafkaMessageEntity kafkaMessageEntity) {
    inputTopicProducerClient.publishSentence(kafkaMessageEntity.getMessagePayload());
  }
}
