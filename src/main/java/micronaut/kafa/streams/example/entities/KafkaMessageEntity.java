package micronaut.kafa.streams.example.entities;

public class KafkaMessageEntity {

  private Integer messageId;
  private String  messagePayload;

  public Integer getMessageId() {
    return messageId;
  }

  public void setMessageId(Integer messageId) {
    this.messageId = messageId;
  }

  public String getMessagePayload() {
    return messagePayload;
  }

  public void setMessagePayload(String messagePayload) {
    this.messagePayload = messagePayload;
  }
}
