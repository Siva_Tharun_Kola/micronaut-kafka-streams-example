package micronaut.kafa.streams.example.controller;

import javax.inject.Inject;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import micronaut.kafa.streams.example.entities.KafkaMessageEntity;
import micronaut.kafa.streams.example.service.KafkaStreamService;

@Controller("/kafka_streams")
public class KafkaStreamController {

  private final KafkaStreamService kafkaStreamService;

  @Inject
  public KafkaStreamController(KafkaStreamService kafkaStreamService) {
    this.kafkaStreamService = kafkaStreamService;
  }

  @Post(value = "/message",consumes = MediaType.APPLICATION_JSON)
  public void sendKafkaMessage(@Body KafkaMessageEntity kafkaMessageEntity) {
    kafkaStreamService.sendKafkaMessage(kafkaMessageEntity);
  }
}
