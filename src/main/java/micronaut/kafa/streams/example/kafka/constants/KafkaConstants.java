package micronaut.kafa.streams.example.kafka.constants;

public interface KafkaConstants {

  public static String INPUT_TOPIC       = "input-text-topic";
  public static String OUTPUT_TOPIC      = "output-word-count-topic";
  public static String STREAM_WORD_COUNT = "word-count";
  public static String WORD_COUNT_STORE  = "word-count-store";

}
