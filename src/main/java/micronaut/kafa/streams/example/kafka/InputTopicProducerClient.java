package micronaut.kafa.streams.example.kafka;

import org.apache.kafka.clients.producer.ProducerConfig;

import io.micronaut.configuration.kafka.annotation.KafkaClient;
import io.micronaut.configuration.kafka.annotation.Topic;
import io.micronaut.context.annotation.Property;
import micronaut.kafa.streams.example.kafka.constants.KafkaConstants;

@KafkaClient(id = "input-topic-producer", acks = KafkaClient.Acknowledge.ALL, properties = @Property(name = ProducerConfig.RETRIES_CONFIG, value = "5"))
public interface InputTopicProducerClient {
  @Topic(KafkaConstants.INPUT_TOPIC)
  void publishSentence(String sentence);
}
