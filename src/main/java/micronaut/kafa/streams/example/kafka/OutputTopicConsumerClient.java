package micronaut.kafa.streams.example.kafka;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.micronaut.configuration.kafka.annotation.KafkaKey;
import io.micronaut.configuration.kafka.annotation.KafkaListener;
import io.micronaut.configuration.kafka.annotation.OffsetReset;
import io.micronaut.configuration.kafka.annotation.OffsetStrategy;
import io.micronaut.configuration.kafka.annotation.Topic;
import micronaut.kafa.streams.example.kafka.constants.KafkaConstants;

@KafkaListener(groupId = "output-topic-consumer-group", offsetReset = OffsetReset.EARLIEST, offsetStrategy = OffsetStrategy.AUTO)
public class OutputTopicConsumerClient {

  private final Map<String, Long> wordCounts = new ConcurrentHashMap<>();

  @Topic(KafkaConstants.OUTPUT_TOPIC)
  void count(@KafkaKey String word, Long count) {
    wordCounts.put(word, count);
  }

  public long getCount(String word) {
    Long num = wordCounts.get(word);
    if (num != null) {
      return num;
    }
    return 0;
  }

  public Map<String, Long> getWordCounts() {
    return Collections.unmodifiableMap(wordCounts);
  }

}
